extends KinematicBody2D

var speed := 0.0
var velocity := Vector2.ZERO
var dir_rotation := 0.0
var direction : Vector2

func _physics_process(delta: float) -> void:
	direction = Vector2.UP

	dir_rotation += Input.get_axis("move_left","move_right") * delta
	speed += Input.get_axis("move_down", "move_up") * delta * 100
	direction = direction.rotated(dir_rotation)

	# Complete this line to move the ship.
	velocity = direction * speed

	move_and_collide(velocity * delta)
	rotation = direction.angle()
