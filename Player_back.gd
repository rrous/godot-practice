extends KinematicBody2D

var velocity : Vector2
var direction : Vector2
var speed = 500

func _process(delta):
	direction = Vector2.ZERO
	direction = Input.get_vector("move_left","move_right","move_up","move_down")
	velocity = speed * direction * delta
	
	rotation = direction.angle()
	
	move_and_collide(velocity)

